module gitlab.com/sgolub/advent-of-code/2020/day-8

go 1.15

require (
	gitlab.com/sgolub/advent-of-code/2020/shared/v2 v2.0.0
	gitlab.com/sgolub/advent-of-code/base/v2 v2.1.0
)
