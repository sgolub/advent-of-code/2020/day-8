package main

import (
	"fmt"

	"gitlab.com/sgolub/advent-of-code/2020/shared/v2/gameconsole"
	"gitlab.com/sgolub/advent-of-code/base/v2/days"
)

const dataKey = "program"

type myDay struct {
	data map[string]string
}

func (d myDay) SetData(key, value string) {
	d.data[key] = value
}

// Year will return the AOC Year
func (d myDay) Year() int {
	return 2020
}

// Day will return the day number for this puzzle
func (d myDay) Day() int {
	return 8
}

// GetData will load the data and parse it from disk
func (d myDay) GetData(useSampleData bool, part int) []*gameconsole.Instruction {
	result := make([]*gameconsole.Instruction, 0)
	data, ok := days.Control().LoadData(d, useSampleData)[fmt.Sprintf("%s_%d", dataKey, part)]
	if !ok {
		return result
	}
	for _, i := range data.([]interface{}) {
		instr, err := gameconsole.NewInstructionFromString(i.(string))
		if err == nil {
			result = append(result, instr)
		}
	}
	return result
}

// Solution1 is the solution to the first part of the puzzle
func (d myDay) Solution1(useSample bool) string {
	data := d.GetData(useSample, 1)
	defer days.NewTimer("D8P1")
	gc := gameconsole.NewHandheldGameConsole(data)
	if d.data["ui"] == "true" {
		gc.RunWithUI()
	}
	return fmt.Sprint(gc.Run())
}

// Solution2 is the solution to the second part of the puzzle
func (d myDay) Solution2(useSample bool) string {
	data := d.GetData(useSample, 2)
	defer days.NewTimer("D8P2")
	gc := gameconsole.NewHandheldGameConsole(data)
	if d.data["ui"] == "true" {
		gc.RunWithUI()
	}
	return fmt.Sprint(gc.Run())
}

// GetDay will return this module's day. Used by the plugin loader.
func GetDay() days.Day {
	return myDay{
		data: map[string]string{},
	}
}
